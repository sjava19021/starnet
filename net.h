#pragma once
#include <vector>



class net
{
public:
	net(const std::vector<unsigned> &topology);
	void feedForward(const std::vector<double> &inputVals);
	void backProp(const std::vector<double> &targetVals);
	void getResults(std::vector<double> &resultVals) const;
	double getRecentAverageError() const { return _recentAverageError; }
	void printTopology();
private:
	std::vector<Layer> layers;//layers[layerNumber][NeuronNumber]
	double error;
	double _recentAverageError;
	static double recentAverageSmoothingFactor;
	std::vector<unsigned> _topology;
};
