#pragma once
#include <vector>
#include <cmath>

class neuron;

typedef std::vector<neuron> Layer;

struct Connection
{
	double weight;
	double deltaWeight;
};

enum activationFunctionType{
	Tanh,
	ReLU,
	Sigmoid,
	Linear
};

class neuron
{
public:
	neuron(unsigned numOutputs, unsigned myIndex);
	void setOutputVal(double val) { _outputVal = val; }
	double getOutputVal() const { return _outputVal; }
	void feedForward(const Layer &prevLayer, activationFunctionType functionType);
	void calcOutputGradients(double targetVals);
	void calcHiddenGradients(const Layer &nextLayer);
	void updateInputWeights(Layer &prevLayer);
private:
	static double _eta;
	static double _alpha;
	static double randomWeight() { return rand() / double(RAND_MAX); }
	static double activationFunction(double x, activationFunctionType func);
	static double activationFunctionDerivative(double x, activationFunctionType func);
	double sumDOW(const Layer &nextLayer) const;
	double _outputVal;
	std::vector<Connection> _outputWeights;
	unsigned _index;
	double _gradient;
};