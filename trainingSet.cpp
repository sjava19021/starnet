#include "trainingSet.h"
#include <algorithm>

trainingSet::trainingSet(const std::string filename)
{
	_trainingDataFile.open(filename.c_str());
	if(!_trainingDataFile.is_open()){
		std::cout << "ERROR! File is not open" << std::endl;
	}

}

void trainingSet::getTopology(std::vector<unsigned> &topology)
{
	std::string line;
	std::string label;

	std::getline(_trainingDataFile, line);
	std::stringstream ss(line);
	ss >> label;
	if (this->isEOF() || label.compare("topology:") != 0)
	{
		abort();
	}

	while (!ss.eof())
	{
		unsigned n;
		ss >> n;
		topology.push_back(n);
	}
	return;
}

void trainingSet::setTopology(std::vector<unsigned> &topology)
{
	_topology = topology;
}

unsigned trainingSet::getNextInputLine(std::vector<double> &inputVals,
						   std::vector<double> &targetOutputVals,
						   const std::set<int> &inputValsIndxs,
						   const std::set<int> &targetOutputValsIndxs,
						   int classCount)
{
	inputVals.clear();
	targetOutputVals.clear();

	for(int i = 0; i < classCount; ++i){
		targetOutputVals.push_back(0);
	}

	std::string line;
	std::getline(_trainingDataFile, line);
	std::stringstream ss(line);

	int i = 0;
	double value;
	std::string valueStr;
	//while (ss >> oneValue) {
	while(std::getline(ss, valueStr, ',')){
		value = ::atof(valueStr.c_str());
		if(inputValsIndxs.find(i) != inputValsIndxs.end())
			inputVals.push_back(value);
		else if(targetOutputValsIndxs.find(i) != targetOutputValsIndxs.end())
			//targetOutputVals.push_back(value);
			targetOutputVals[value] = 1;
		i++;
	}

	return i;
}

/**
 * Read the first line from the file. 
 * The words should be separated by commas.
*/
void trainingSet::readCsvHeader(std::map<std::string, int> &header){
	std::string line;
	std::string token;
	char sep = ',';

	std::getline(_trainingDataFile, line); 
    std::stringstream ss(line);

	int i = 0;
    while (getline(ss, token, sep)) {
        token.erase(remove_if(token.begin(), token.end(), ::isspace), token.end());
        if (!token.empty())
            header[token] = i++;
    }
	return;
}