#include "neuron.h"
#include <algorithm>
#include <cassert>


double neuron::_eta = 0.15; // net learning rate
double neuron::_alpha = 0.5; // momentum

neuron::neuron(unsigned numOutputs, unsigned index) : _index(index)
{
	for (unsigned i = 0; i < numOutputs; ++i) {
		_outputWeights.push_back(Connection());
		_outputWeights.back().weight = randomWeight();
	}
}

void neuron::updateInputWeights(Layer &prevLayer)
{
	for (unsigned n = 0; n < prevLayer.size(); ++n)
	{
		neuron &neuron = prevLayer[n];
		double oldDeltaWeight = neuron._outputWeights[_index].deltaWeight;

		double newDeltaWeight = _eta * neuron.getOutputVal() * _gradient + _alpha * oldDeltaWeight;
		neuron._outputWeights[_index].deltaWeight = newDeltaWeight;
		neuron._outputWeights[_index].weight += newDeltaWeight;
	}
}

double neuron::sumDOW(const Layer &nextLayer) const
{
	double sum = 0.0;

	for (unsigned i = 0; i < nextLayer.size() - 1; ++i)
	{
		sum += _outputWeights[i].weight * nextLayer[i]._gradient;
	}

	return sum;
}

void neuron::calcHiddenGradients(const Layer &nextLayer)
{
	double dow = sumDOW(nextLayer);
	_gradient = dow * neuron::activationFunctionDerivative(_outputVal, activationFunctionType::Tanh);
}
void neuron::calcOutputGradients(double targetVals)
{
	double delta = targetVals - _outputVal;
	_gradient = delta * neuron::activationFunctionDerivative(_outputVal, activationFunctionType::Tanh);
}

double neuron::activationFunction(double x, activationFunctionType func)
{
	switch (func)
	{
	case Tanh:
		//output range [-1.0..1.0]
		return tanh(x);
	case ReLU:
		return std::max(0.0, x);
	case Sigmoid:
		//return 1 / (1 + 1/std:);
		break;
	case Linear:
		return x;
	default:
		assert(0);
		break;
	}
	assert(0);
	return 0.0;
}

double neuron::activationFunctionDerivative(double x, activationFunctionType func)
{
	switch (func)
	{
	case Tanh:
		return 1.0 - tanh(x) * tanh(x);	
	case Linear:
		return 1;
	default:
		break;
	}
	assert(0);
	return 0.0;
}

void neuron::feedForward(const Layer &prevLayer, activationFunctionType activationFunction)
{
	double sum = 0.0;

	for (unsigned i = 0; i < prevLayer.size(); ++i)
	{
		sum += prevLayer[i].getOutputVal() * 
			prevLayer[i]._outputWeights[_index].weight;
	}

	_outputVal = neuron::activationFunction(sum, activationFunction);
}