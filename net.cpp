#include "trainingSet.h"
#include "neuron.h"
#include "net.h"


double net::recentAverageSmoothingFactor = 100.0; // Number of training samples to average over

net::net(const std::vector<unsigned> &topology) : _topology(topology)
{
	unsigned numLayers = topology.size();
	for (unsigned layerNum = 0; layerNum < numLayers; ++layerNum) {
		layers.push_back(Layer());
		//количество выходов: либо 0(если это последний слой), либо количество нейронов в следующем слое
		unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

		for (unsigned neuronNumber = 0; neuronNumber <= topology[layerNum]; ++neuronNumber)
			layers.back().push_back(neuron(numOutputs, neuronNumber));

		//bias
		layers.back().back().setOutputVal(1.0);
	}
}


void net::getResults(std::vector<double> &resultVals) const
{
	resultVals.clear();

	for (unsigned i = 0; i < layers.back().size() - 1; ++i)
	{
		resultVals.push_back(layers.back()[i].getOutputVal());
	}
}

void net::backProp(const std::vector<double> &targetVals)
{
	Layer &outputLayer = layers.back();
	error = 0.0;

	//цикл по всем нейронам выходного слоя
	for (unsigned n = 0; n < outputLayer.size() - 1; ++n)
	{
		double delta = targetVals[n] - outputLayer[n].getOutputVal();
		error += delta * delta;
	}
	error /= outputLayer.size() - 1; 
	error = sqrt(error); 


	_recentAverageError =
		(_recentAverageError * recentAverageSmoothingFactor + error)
		/ (recentAverageSmoothingFactor + 1.0);


	for (unsigned i = 0; i < outputLayer.size() - 1; ++i)
	{
		outputLayer[i].calcOutputGradients(targetVals[i]);
	}

	for (unsigned layerNum = layers.size() - 2; layerNum > 0; --layerNum)
	{
		Layer &hiddenLayer = layers[layerNum];
		Layer &nextLayer = layers[layerNum + 1];

		for (unsigned i = 0; i < hiddenLayer.size(); ++i)
		{
			hiddenLayer[i].calcHiddenGradients(nextLayer);
		}
	}


	for (unsigned layerNum = layers.size() - 1; layerNum > 0; --layerNum)
	{
		Layer &layer = layers[layerNum];
		Layer &prevLayer = layers[layerNum - 1];

		for (unsigned n = 0; n < layer.size() - 1; ++n)
		{
			layer[n].updateInputWeights(prevLayer);
		}
	}
}

void net::feedForward(const std::vector<double> &inputVals)
{
	assert(inputVals.size() == layers[0].size() - 1);

	for (unsigned i = 0; i < inputVals.size(); ++i)
		layers[0][i].setOutputVal(inputVals[i]);

	for (unsigned layerNum = 1; layerNum < layers.size(); ++layerNum) {
		Layer &prevLayer = layers[layerNum - 1];
		for (unsigned i = 0; i < layers[layerNum].size() - 1; ++i)
			layers[layerNum][i].feedForward(prevLayer, activationFunctionType::Tanh);
	}

	//last layer
	//Layer &prevLayer = layers[layers.size()-2];
	//for (unsigned i = 0; i < layers[layers.size()-1].size() - 1; ++i)
	//	layers[layers.size()-1][i].feedForward(prevLayer, activationFunctionType::Tanh);
}

void net::printTopology(){
	for(int i = 0; i < _topology.size(); ++i)
		std::cout << "Layer_" << i << "; neurons: " << _topology[i] << std::endl;
}

