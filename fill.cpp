#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>


int main()
{
	std::ofstream file;
	file.open("testData.csv");

	if(file.is_open()){
		file << "x1" << "," << "x2" << "," << "y" << std::endl; 
		for (int i = 20000; i >= 0; i--)
		{
			int n1 = (int)(2.0 * rand() / double(RAND_MAX));
			int n2 = (int)(2.0 * rand() / double(RAND_MAX));
			int t = !(n1 & n2);

			file << n1 << ".0" << "," << n2 << ".0" << "," << t << ".0" << std::endl;
		}
	}
}