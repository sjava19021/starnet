#pragma once
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include <map>
#include <set>

class trainingSet
{
public:
	trainingSet(const std::string filename);
	bool isEOF() { return _trainingDataFile.eof(); }
	void seekg() { _trainingDataFile.seekg(std::ios::beg);}
	void getTopology(std::vector<unsigned> &topology);
	void setTopology(std::vector<unsigned> &topology);
	void readCsvHeader(std::map<std::string, int> &header);
	unsigned getNextInputLine(std::vector<double> &inputVals,
							   std::vector<double> &targetOutputVals,
							   const std::set<int> &inputValsIndxs,
							   const std::set<int> &targetOutputValsIndxs,
							   int classCount);
private:
	std::vector<unsigned> _topology;
	std::ifstream _trainingDataFile;
};