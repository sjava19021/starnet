#include "trainingSet.h"
#include "neuron.h"
#include "net.h"
#include <map>
#include <set>
#include <time.h>



constexpr int LOG_DELAY = 10000;
constexpr int EPOCHS = 2;

void showVectorVals(std::string label, std::vector<double> &v)
{
	std::cout << label << " ";
	for (unsigned i = 0; i < v.size(); i++)
	{
		std::cout << v[i] << " ";
	}
	std::cout << std::endl;
}

int main()
{
	//открываем файл с тестами
	trainingSet trainingData("/home/sjava/starnet/archive/mnist_train.csv");
	//элементы вектора topology - количество нейронов в каждом слое
	std::vector<unsigned> topology = {28*28, 32, 10};
	std::map<std::string, int> header;
	trainingData.readCsvHeader(header);


	//fill indxs of input, target columns
	std::set<int> inputIndxs;
	std::set<int> targetIndxs;
	targetIndxs.insert(0);
	for(int i = 1; i <= 28*28; ++i){
		inputIndxs.insert(i);
	}


	net net(topology);

	std::vector<double> inputVals, targetVals, resultVals;
	int trainingPass = 0;

	clock_t start = clock(); 
	clock_t end = clock(); 
	for(int epoch = 1; epoch <= EPOCHS; ++epoch){
		std::cout << "EPOCH >>>> "  << epoch << " <<<<" << std::endl << std::endl;
 		while (true)
		{
			
			++trainingPass;

			const int classCount = 10;
			if(!trainingData.getNextInputLine(inputVals, targetVals, inputIndxs, targetIndxs, classCount))
				break; //EOF
			
			assert(inputVals.size() == topology[0]);

			//showVectorVals("Input:", inputVals);
			net.feedForward(inputVals);

			//std::cout << targetVals.size() << "  "  << topology.back() << std::endl;
			assert(targetVals.size() == topology.back());

			net.getResults(resultVals);

			net.backProp(targetVals);


			if(trainingPass % LOG_DELAY == 0){
				end = clock();
				double seconds = (double)(end - start) / CLOCKS_PER_SEC;
				start = clock();
				std::cout << "//==============================//" << std::endl;
				std::cout << std::endl << "Pass: " << trainingPass << ";   time: " << seconds << std::endl;
				showVectorVals("Targets:", targetVals);
				showVectorVals("Outputs", resultVals);
				std::cout << "Net average error: " << net.getRecentAverageError() << std::endl;
			}
		}
		trainingData.seekg();
	}
	

	std::cout << std::endl << "Done" << std::endl;
#if defined(_MSC_VER) || defined(_WIN32)
	system("PAUSE");
#endif
	return(0);
}